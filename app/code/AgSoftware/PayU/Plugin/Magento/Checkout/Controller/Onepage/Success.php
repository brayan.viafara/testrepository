<?php

declare(strict_types=1);

namespace AgSoftware\PayU\Plugin\Magento\Checkout\Controller\Onepage;

use Magento\Sales\Api\OrderRepositoryInterface;
use mysql_xdevapi\Exception;
use function PHPUnit\Framework\throwException;

class Success
{
  private $data = [];

  public function __construct(
      \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
    \Magento\Checkout\Model\Session $checkoutSession,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeInterface,
    \Magento\Framework\View\Layout $layout,
    \Magento\Framework\View\Result\PageFactory $resultPageFactory,
    \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
      ) {
    $this->orderRepository =$orderRepository;
    $this->resultPageFactory = $resultPageFactory;
    $this->layout = $layout;
    $this->scopeInterface = $scopeInterface;
    $this->checkoutSession = $checkoutSession;
    $this->data['referenceCode'] = 'bryx'.$this->checkoutSession->getLastOrderId();
    $this->redirectFactory = $redirectFactory;
  }

  public function getModuleConfig($path, $storeId = null)
  {
    return $this->scopeInterface->getValue(
      "payment/payu/" . $path,
      \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
      $storeId
    );
  }

  public function signature($apikey, $merchantId, $reference, $amount, $currency)
  {
    $out = md5("$apikey~$merchantId~$reference~$amount~$currency");
    $this->data['siganture'] = $out;
    return $out;
  }

  private function getData()
  {
    $order = $this->checkoutSession->getLastRealOrder();
    if ($order->getId()) {
      $data = $order->getData();
      $this->data['email'] = $data['customer_email'];
      $this->data['currency'] = 'USD';
      $total=round($data['grand_total'], 2);
      $this->data['amount'] = $total /*$data['grand_total'];*/ ;
      $this->data['taxReturnBase'] = $data['tax_refunded'];
      $this->data['apikey'] = $this->getModuleConfig('api');
      $this->data['merchantId'] = $this->getModuleConfig('merchantId');
      $this->data['accountId'] = $this->getModuleConfig('accountId');
       // print_r($data);
    }
   //throw new \Exception(json_encode($data));
                //print_r("<pre>");
                //$order=$this->checkoutSession->getLastRealOrder()->getPayment()->getData();
      //throw new \Exception(json_encode($order));
                // print_r($order["method"]);
   //print_r(get_class_methods($this->checkoutSession->getLastRealOrder()));
   //print_r($order->isPaymentReview());
                // print_r("</pre>");
    //die();
    // return $data;

  }
  public function getPayMethod (){
      $data=$this->orderRepository->get($this->checkoutSession->getLastOrderId());
      $thos=$this;
      if ($data->getId()){
          $method= $data->getPayment()->getData();
          $payMethod=$method['method'];
         $this->data['payMethod'] = $payMethod;
          }
      return $thos->payMethod;

  }

    public function afterExecute(
    \Magento\Checkout\Controller\Onepage\Success $subject,
    $result
  ) {
        $this->getPayMethod();
        $paymethod= $this->getPayMethod();
     //if (true){
     //  throw new \Exception(json_encode($paymethod));

          if ($paymethod === "payu") {
              $this->getData();
           //   throw new \Exception(json_encode($paymethod));
              $apikey = $this->getModuleConfig('api');
              $merchantId = $this->getModuleConfig('merchantId');
             // $accountId = $this->getModuleConfig('accountId');
              $reference = $this->data['referenceCode'];
              $amount = $this->data['amount'];
              $currency = $this->data['currency'];
              $signature = $this->signature($apikey, $merchantId, $reference, $amount, $currency);
              $this->data['signature'] = $signature;
           
              //throw new \Exception(get_class($layout));
              $blocks = $layout->getBlock('agsoftware.payu.payu');
              //print_r($blocks);
              $blocks->setData($this->data);
              return $result;
          }
       else {
          throw new \Exception(json_encode($paymethod));
           $redirect = $this->redirectFactory->create();
           $redirect->setPath('https://magento.test/payu/confirmation/completado/');
           return $redirect;
           }
   //}

  }
}
