<?php
 
namespace AgSoftware\YerriMina\Setup\Patch\Data;

class Bloques2 implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * CreateHeaderBlock constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Cms\Model\BlockRepository $blockRepository
     * @param \Magento\Cms\Api\Data\BlockInterface $block
     */

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Cms\Model\BlockRepository $blockRepository,
        \Magento\Cms\Api\Data\BlockInterfaceFactory $block,
        \Magento\Cms\Api\GetBlockByIdentifierInterface $blockByIdentifier
    ) {
        $this->blockRepository = $blockRepository;
        $this->block = $block;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->blockByIdentifier = $blockByIdentifier;
    }
    /**
     * {@inheritdoc}
     */
    public function apply()
    { 
        //$this->moduleDataSetup->startSetup();
        
        $this->moduleDataSetup->getConnection()->startSetup();

        
        //code
 /*
        $block_data = [
            'title' => 'header',
            'identifier' => 'header',
            'is_active' => 1,
            'content' => file_get_contents(__DIR__.'/html/header.html'),
        ];
        $block = $this->block->create();
        $block->addData($block_data);
        $block->setStores([0]);
        $this->blockRepository->save($block);
        //$this->moduleDataSetup->endSetup();
*/
        $block_data = [
            'title' => 'articule',
            'identifier' => 'articule',
            'is_active' => 1,
            'content' => file_get_contents(__DIR__.'/html/articule.html'),
        ];
        $block = $this->block->create();
        $block->addData($block_data);
        $block->setStores([0]);
        $this->blockRepository->save($block);
        //$this->moduleDataSetup->endSetup();
        $this->moduleDataSetup->getConnection()->endSetup();
    }

      /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }
    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
    /**
     * Revert patch
     */
    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        //code
        $this->moduleDataSetup->getConnection()->endSetup();
    }
}