<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AgSoftware\PayU\Controller\Confirmation;

class Pagopayu extends \Magento\Framework\App\Action\Action
{

    private $data = [];
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository

    ) {
        $this->orderRepository = $orderRepository;
        $this->checkoutSession = $checkoutSession;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        /*
        $data = $this->getRequest()->getParams();
        $pageResult =$this->resultPageFactory->create();
        if(str_replace('bryx','',$data['referenceCode']) == $this->checkoutSession->getLastOrderId()){
            $layout=$pageResult->getLayout();
            // throw new \Exception(get_class($layout));
            $block =$layout->getBlock('agsoftware.payu.confirmation');
            $block->setData($data);
            // $this->data = $data;
            $orderId=$this->checkoutSession->getLastOrderId();
            $order = $this->orderRepository->get($orderId);
           // throw new \Exception(json_encode(get_class_methods($this->orderRepository)));
            ///throw new \Exception(json_encode($order->getData()));
            $order->setStatus('processing');
            $order->setState('processing');
            $order->save();
            // echo "hola ".$this->data['email'];
            //get_class_methods($this);
            //  die;
        }
        */
        //throw new \Exception(json_encode([str_replace('bryx','',$data['referenceCode']), $this->checkoutSession->getLastOrderId()]));
        return  ;
    }
}
