define(["ko", "uiComponent", "jquery"], function (ko, Component, jq) {
    "use strict";
    let time = {
        _currenTime: ko.observable("cargando..."),
        // _cambioName: ko.observable("value"),
        initialize: function () {
            this._super();
            setInterval(this.updateTime.bind(this), 1000)
           //setInterval(this._getName.bind(this), 2000)
        },
        // _getName:function(){
        //    jq("#nombre").innerHTML="hello";
        },
     
        getTime: function (){
            return this._currenTime;
        },
        updateTime:function(){
            this._currenTime(new Date);
        }
    }
return Component.extend(time);
});
