<?php
declare(strict_types=1);

namespace AgSoftware\PayU\Plugin\Magento\Checkout\Controller\Onepage;

class Success
{

    public function afterExecute(
        \Magento\Checkout\Controller\Onepage\Success $subject,
        $result
    ) {
        //Your plugin code
     

        return $result;
    }
}
