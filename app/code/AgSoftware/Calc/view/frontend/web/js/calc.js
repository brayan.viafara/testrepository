define(["jquery", "uiComponent"], function ($, Component) {
  "use strict";
  //code

  var Calc = {
    initialize: function () {
      this._super();
    },
    _click: function (component, boton) {
      let data = this.pantalla.val();
      this.pantalla.val(data + boton.currentTarget.value);

      //console.log("_click", boton.currentTarget.value);
    },
    _result: function () {
      let valueDisplay = this.pantalla.val();
      if (
        valueDisplay !== "" &&
        valueDisplay !== "/" &&
        valueDisplay !== "-" &&
        valueDisplay !== "*" &&
        valueDisplay !== "+"
      ) {
        let data = this.pantalla.val();
        this.pantalla.val(eval(data));
      } else {
        alert("verifica tu operacion");
      }
    },
    pantalla: $("#pantalla"),

    _borrar: function () {
      let data = this.pantalla.val();
      let delet = data.substring(0, data.length - 1);
      this.pantalla.val(delet);
    },
  };

  return Component.extend(Calc);
});
